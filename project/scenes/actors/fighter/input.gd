class_name InputHandler extends Node

@export var state_chart := StateChart.new()

const FRAMES_PER_SECOND := 60
const BUFFER_SIZE = 10

var buffer := []

func _ready() -> void:
	buffer.resize(BUFFER_SIZE)

func _process(_delta: float) -> void:
		do_buffer()
		if buffer.count(6) >= 2:
			state_chart.send_event("dash")
		if buffer.count(4) >= 2:
			state_chart.send_event("dash")

func do_buffer() -> void:
	if Input.is_action_just_pressed("move_right"):
		buffer.push_front(6)
	elif Input.is_action_just_pressed("move_left"):
		buffer.push_front(4)
	elif Input.is_action_just_pressed("move_down"):
		buffer.push_front(2)
	elif Input.is_action_just_pressed("move_up"):
		buffer.push_front(8)
	else:
		buffer.push_front(5)
	buffer.pop_back()

func has_623() -> bool:
	var index := buffer.find(6)
	if index < 0:
		index = buffer.find(4)
		
	if index >= 0:
		if buffer.find(2, index) > -1:
			return true
	return false
