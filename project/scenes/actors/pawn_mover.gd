class_name PawnMover extends CharacterBody3D

@onready var state_chart: StateChart = $StateChart
@onready var pivot: Node3D = $Pivot

var facing := Vector2.RIGHT

var input_direction := Vector2.ZERO

var movement_enabled := true
var can_break := true

#@export_group("Movement")
@export var move_speed := 300.0
@export_range(0.0, 1.0, 0.05) var break_force := 0.10

@export var jump_height : float
@export var jump_time_to_peak : float
@export var jump_time_to_descent : float

@onready var jump_velocity : float = ((2.0 * jump_height) / jump_time_to_peak)# * -1.0
@onready var jump_gravity : float = ((-2.0 * jump_height) / (jump_time_to_peak * jump_time_to_peak))# * -1.0
@onready var fall_gravity : float = ((-2.0 * jump_height) / (jump_time_to_descent * jump_time_to_descent))# * -1.0


func _physics_process(delta: float) -> void:
	if not movement_enabled:
		return

	# Add the gravity.
	if not is_on_floor():
		velocity.y += _get_gravity() * delta
	
	var move_direction := (transform.basis * Vector3(input_direction.x, 0, input_direction.y)).normalized()
	
	if move_direction: # Move
		velocity.x = move_direction.x * move_speed
		velocity.z = move_direction.z * move_speed
		if move_direction.x > 0:
			facing = Vector2.RIGHT
			pivot.scale.x = 1.0
		elif move_direction.x < 0:
			facing = Vector2.LEFT
			pivot.scale.x = -1.0
	else: 
		# Brake
		if is_on_floor() and can_break:
			velocity.x = move_toward(velocity.x, 0, move_speed * break_force)
			velocity.z = move_toward(velocity.z, 0, move_speed * break_force)
	
	move_and_slide()

func jump(_lateral_modifier:float=1.0) -> void:
	velocity.y = jump_velocity

func _get_gravity() -> float:
	return jump_gravity if velocity.y > 0.0 else fall_gravity
