class_name Enemy extends PawnMover

const DISTANCE_TOLERANCE = 1.1

@onready var attack_radius: Area3D = $AttackRadius
@onready var animator: AnimationPlayer = $AnimationPlayer

var state

## Attempts moving to a set destination.
## Returns 0 if in progress, 1 if success, -1 if failure.
func attempt_walk_to(destination: Vector3) -> int:
	if global_position.distance_to(destination) <= DISTANCE_TOLERANCE:
		input_direction = Vector2.ZERO
		return 1
	else:
		var _input := global_position.direction_to(destination)
		input_direction = Vector2(_input.x, _input.z).normalized()
		return 0

func attempt_attack() -> int:
	animator.play("attack")
	return 1

func take_damage() -> void:
	animator.play("hurt")


func _on_hurtbox_area_entered(_area: Area3D) -> void:
	take_damage()
