class_name Fighter extends PawnMover
"""
Input considerations. 
One idea is to just send signals for inputs to the state machine agnostically,
and let transitions sort it out i.e. sending attack signal whenever, but it only
gets 'observed' during states that use it.
"""
@onready var sprite: Sprite3D = $Pivot/Sprite3D
@onready var input_buffer: InputHandler = $INPUT
@onready var redash: Timer = $ReDash


#region Movement State
func _on_movement_state_processing(_delta: float) -> void:
	input_direction = Input.get_vector("move_left", "move_right", "move_up", "move_down")
	if input_direction:
		if input_direction.x > 0:
			sprite.flip_h = false
		elif input_direction.x < 0:
			sprite.flip_h = true

		
		state_chart.send_event("moving_start")
	else:
		state_chart.send_event("moving_stop")
	
	if Input.is_action_just_pressed("jump"):
		state_chart.send_event("jump")
		jump()
	
	if Input.is_action_just_pressed("attack"):
		if input_buffer.has_623():
			state_chart.send_event("623A")
		else:
			state_chart.send_event("attack")

func _on_movement_state_exited() -> void:
	input_direction = Vector2.ZERO
#endregion

#region Slide State
func _on_slide_state_entered() -> void:
	state_chart.set_expression_property("can_dash", false)
	redash.start()
	
	velocity.x = facing.x * move_speed * 4.5
	print("Dash!")
	
#endregion
func _on_stand_state_entered() -> void:
	break_force = 0.30

func _on_stand_state_exited() -> void:
	break_force = 0.10
#region Idle State
#endregion

#region PreJump Sate
#func _on_pre_jump_state_entered() -> void:
	#input_direction = Vector2.ZERO
	#can_break = false
#
#func _on_pre_jump_state_exited() -> void:
	#can_break = true
#endregion

#region Airborn State
func _on_airborn_state_physics_processing(_delta: float) -> void:
	if is_on_floor():
		state_chart.send_event("grounded")
	
	if Input.is_action_just_pressed("jump") and is_on_wall_only():
		facing *= -1
		pivot.scale.x *= -1
		sprite.flip_h = not sprite.flip_h
		velocity.x = move_speed * facing.x
		jump()
		state_chart.send_event("walljump")
	
	if Input.is_action_just_pressed("attack"):
		state_chart.send_event("attack")
	
	if velocity.y < 0:
		state_chart.send_event("falling")
#endregion

#region Attack State
func _on_attack_state_entered() -> void:
	pass # Replace with function body.

func _on_attack_state_processing(_delta: float) -> void:
	if Input.is_action_just_pressed("attack"):
		state_chart.set_expression_property("attack_buffered", true)

func _on_attack_state_exited() -> void:
	state_chart.set_expression_property("attack_buffered", false)
#endregion

func _on_special_1_state_entered() -> void:
	velocity = Vector3.ZERO

func _on_animation_player_animation_finished(_anim_name: StringName) -> void:
	state_chart.send_event("animation_finished")


func _on_re_dash_timeout() -> void:
	state_chart.set_expression_property("can_dash", true)
